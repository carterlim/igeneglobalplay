package models;

import java.util.List;
import javax.persistence.*;
import play.data.validation.Constraints;

@Entity
public class Droplet {

  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
	public Long id;

  @Constraints.Required
  protected String name;

  @Constraints.Required
  protected String image;

  @Constraints.Required
  protected String size;

  @Constraints.Required
  protected String region;

  @Constraints.Required
  protected String site;

  public void setName(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public String getImage() {
    return image;
  }

  public void setSize(String size) {
    this.size = size;
  }

  public String getSize() {
    return size;
  }

  public void setRegion(String region) {
    this.region = region;
  }

  public String getRegion() {
    return region;
  }

  public void setSite(String site) {
    this.site = site;
  }

  public String getSite() {
    return site;
  }
}
