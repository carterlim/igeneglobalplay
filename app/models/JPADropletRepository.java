package models;

import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.util.concurrent.CompletableFuture.supplyAsync;

/**
 * Provide JPA operations running inside of a thread pool sized to the connection pool
 */
public class JPADropletRepository implements DropletRepository {

    private final JPAApi jpaApi;
    private final DatabaseExecutionContext executionContext;

    @Inject
    public JPADropletRepository(JPAApi jpaApi, DatabaseExecutionContext executionContext) {
        this.jpaApi = jpaApi;
        this.executionContext = executionContext;
    }

    @Override
    public CompletionStage<Droplet> add(Droplet droplet) {
        return supplyAsync(() -> wrap(em -> insert(em, droplet)), executionContext);
    }

    @Override
    public CompletionStage<Stream<Droplet>> list() {
        return supplyAsync(() -> wrap(em -> list(em)), executionContext);
    }

    private <T> T wrap(Function<EntityManager, T> function) {
        return jpaApi.withTransaction(function);
    }

    private Droplet insert(EntityManager em, Droplet droplet) {
        em.persist(droplet);
        return droplet;
    }

    private Stream<Droplet> list(EntityManager em) {
        List<Droplet> droplets = em.createQuery("select d from Droplet d", Droplet.class).getResultList();
        return droplets.stream();
    }
}
