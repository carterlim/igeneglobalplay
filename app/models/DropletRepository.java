package models;

import com.google.inject.ImplementedBy;

import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

/**
 * This interface provides a non-blocking API for possibly blocking operations.
 */
@ImplementedBy(JPADropletRepository.class)
public interface DropletRepository {

    CompletionStage<Droplet> add(Droplet droplet);

    CompletionStage<Stream<Droplet>> list();
}
