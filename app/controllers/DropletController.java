package controllers;

import models.Droplet;
import models.DropletRepository;
import play.data.Form;
import play.data.FormFactory;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.concurrent.*;
import java.util.stream.Collectors;

import views.html.*;

import static play.libs.Json.toJson;

/**
 * The controller keeps all database operations behind the repository, and uses
 * {@link play.libs.concurrent.HttpExecutionContext} to provide access to the
 * {@link play.mvc.Http.Context} methods like {@code request()} and {@code flash()}.
 */
public class DropletController extends Controller {

    private final FormFactory formFactory;
    private final DropletRepository dropletRepository;
    private final HttpExecutionContext ec;

    @Inject WebJarAssets webJarAssets;

    @Inject
    public DropletController(FormFactory formFactory, DropletRepository dropletRepository, HttpExecutionContext ec) {
        this.formFactory = formFactory;
        this.dropletRepository = dropletRepository;
        this.ec = ec;
    }

    public Result index() {
        return ok(views.html.index.render(webJarAssets));
    }

    public Result createDroplet() {
        return ok(views.html.create.render(webJarAssets));
    }

    public CompletionStage<Result> addDroplet() {
        Form<Droplet> dropletForm = formFactory.form(Droplet.class);
        if (dropletForm.hasErrors()) {
            return CompletableFuture.completedFuture(badRequest(views.html.create.render(webJarAssets)));
        } else {
          Droplet droplet = dropletForm.bindFromRequest().get();
          return dropletRepository.add(droplet).thenApplyAsync(d -> {
            return redirect(routes.DropletController.index());
          }, ec.current());
        }
    }

    public CompletionStage<Result> getDroplets() {
        return dropletRepository.list().thenApplyAsync(dropletStream -> {
            return ok(toJson(dropletStream.collect(Collectors.toList())));
        }, ec.current());
    }

}
