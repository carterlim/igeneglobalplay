function Site(props) {
  return (
    <div key={props.id} className={'site site'+props.size+props.selected} onClick={props.onDataCenterRegionClick}>
      <div>{props.value}</div>
    </div>
  );
}

class DataCenterRegion extends React.Component {
  handleDataCenterRegionClick(region, site) {
    this.props.onDataCenterRegionClick(region, site);
  }

  render() {
    var sites = this.props.site.map((item, index) => {
      return (
        <Site
          key={item}
          id={item}
          value={item}
          size={this.props.site.length}
          selected={this.props.selected && this.props.selectedSite == item ? ' selected' : ''}
          onDataCenterRegionClick={() => this.handleDataCenterRegionClick(this.props.id, item)}
        />
      )
    });

    return (
      <div key={this.props.id} className="item-list">
        <div className={'data-center-region'+this.props.selected}>{this.props.value}</div>
        <div>
          {sites}
        </div>
      </div>
    );
  }
}

class DataCenterRegionPanel extends React.Component {
  handleDataCenterRegionClick(region, site) {
    this.props.onDataCenterRegionClick(region, site);
  }

  render() {
    var dataCenterRegions = this.props.list.map((item, index) => {
      return (
        <DataCenterRegion
          key={item.id}
          id={item.id}
          value={item.name}
          site={item.site}
          selectedSite={this.props.site}
          selected={this.props.region == item.id ? ' selected' : ''}
          onDataCenterRegionClick={this.handleDataCenterRegionClick.bind(this)}
        />
      )
    });

    return (
      <div className="data-center-region-panel">
        <div className="page-header">
          <h3>Choose Data a Center Region</h3>
        </div>
        <div className="radio-group">
          {dataCenterRegions}
        </div>
      </div>
    );
  }
}

function Size(props) {
  return (
    <div key={props.id} className={'radio border'+props.selected} onClick={props.onSizeClick}>
      <div>{props.value}</div>
    </div>
  );
}

function SizeTab(props) {
  return (
    <div key={props.id} className={props.selected} onClick={props.onSizeTabChange}>
      <div>{props.value}</div>
    </div>
  );
}

class SizePanel extends React.Component {
  handleSizeClick(data) {
    this.props.onSizeClick(data);
  }

  handleSizeTabChange(data) {
    this.props.onSizeTabChange(data);
  }

  render() {
    var tabs = this.props.list.map((item, index) => {
      return (
        <SizeTab
          key={item.id}
          id={item.id}
          value={item.name}
          selected={this.props.tabIndex == index ? 'selected' : ''}
          onSizeTabChange={() => this.handleSizeTabChange(index)}
        />
      )
    });

    var members = this.props.list[this.props.tabIndex].value.map((item, index) => {
      return (
        <Size
          key={item.id}
          id={item.id}
          value={item.capacity}
          selected={this.props.value == item.capacity ? ' selected' : ''}
          onSizeClick={() => this.handleSizeClick(item.capacity)}
        />
      );
    });

    return (
      <div className="image-panel">
        <div className="page-header tab">
          <h3>Choose Size</h3>
          <div className="panel-tab">
            {tabs}
          </div>
        </div>
        <div className="radio-group">
          {members}
        </div>
      </div>
    );
  }
}

function Image(props) {
  return (
    <div key={props.id} className={'radio border'+props.selected} onClick={props.onImageClick}>
      <div className="glyphicon glyphicon-cloud"></div>
      <div>{props.value}</div>
    </div>
  );
}

function ImageTab(props) {
  return (
    <div key={props.id} className={props.selected} onClick={props.onImageTabChange}>
      <div>{props.value}</div>
    </div>
  );
}

class ImagePanel extends React.Component {
  handleImageClick(data) {
    this.props.onImageClick(data);
  }

  handleImageTabChange(data) {
    this.props.onImageTabChange(data);
  }

  render() {
    var tabs = this.props.list.map((item, index) => {
      return (
        <ImageTab
          key={item.id}
          id={item.id}
          value={item.name}
          selected={this.props.tabIndex == index ? 'selected' : ''}
          onImageTabChange={() => this.handleImageTabChange(index)}
        />
      )
    });

    var members = this.props.list[this.props.tabIndex].value.map((item, index) => {
      return (
        <Image
          key={item}
          id={item}
          value={item}
          selected={this.props.value == item ? ' selected' : ''}
          onImageClick={() => this.handleImageClick(item)}
        />
      );
    });

    return (
      <div className="size-panel">
        <div className="page-header tab">
          <h3>Choose Image</h3>
          <div className="panel-tab">
            {tabs}
          </div>
        </div>
        <div className="radio-group">
          {members}
        </div>
      </div>
    );
  }
}

class Droplet extends React.Component {
  constructor() {
    super();
    this.state = {
      imageTab: 0,
      image: "",
      sizeTab: 0,
      size: "",
      region: "",
      site: "",
      additionalOptions: ""
    };
  }

  handleSizeClick(data) {
    this.setState({size: data});
  }

  handleImageClick(data) {
    this.setState({image: data});
  }

  handleDataCenterRegionClick(region, site) {
    this.setState({region: region});
    this.setState({site: site});
  }

  handleSizeTabChange(data) {
    this.setState({sizeTab: data});
  }

  handleImageTabChange(data) {
    this.setState({imageTab: data});
  }

  render() {
    var images = [{
      id: 'dts',
      name: 'Distributions',
      value: ['Ubuntu', 'FreeBSD', 'Fedora', 'Debian', 'CoreOS', 'CentOS']
    }, {
      id: 'oca',
      name: 'One-click apps',
      value: ['OCA1', 'OCA2', 'OCA3']
    }];

    var sizes = [{
      id: 'std',
      name: 'Standard',
      value: [{id: '1', capacity: '1 GB', price: 5}, {id: '2', capacity: '2 GB', price: 10}, {id: '4', capacity: '4 GB', price: 20}, {id: '8', capacity: '8 GB', price: 40}, {id: '16', capacity: '16 GB', price: 80}]
    }, {
      id: 'hm',
      name: 'High Memory',
      value: [{id: '100', capacity: '100 GB', price: 500}, {id: '200', capacity: '200 GB', price: 1000}, {id: '400', capacity: '400 GB', price: 2000}, {id: '800', capacity: '800 GB', price: 4000}]
    }];

    var dataCenterRegions = [{
      id: 'NY',
      name: 'New York',
      site: ['1', '2', '3']
    }, {
      id: 'SF',
      name: 'San Francisco',
      site: ['1', '2']
    }, {
      id: 'AM',
      name: 'Amsterdam',
      site: ['2', '3']
    }, {
      id: 'SG',
      name: 'Singapore',
      site: ['1']
    }];

    var additionalOptions = [{
      id: 'PN',
      name: 'Private networking'
    }, {
      id: 'BK',
      name: 'Backups'
    }, {
      id: 'IPv6',
      name: 'IPv6'
    }, {
      id: 'UD',
      name: 'User data'
    }, {
      id: 'MT',
      name: 'Monitoring'
    }].map((item, index) => {
      return (
        <div key={item.id} className="radio">
          <input type="checkbox" name="additionalOptions" value={item.id}/>&nbsp;<span>{item.name}</span>
        </div>
      );
    });

    return (
      <div className="droplet">
        <div className="page-header">
          <h1>Create Droplet</h1>
          <div className="droplet-image">
            <ImagePanel
              tabIndex={this.state.imageTab}
              value={this.state.image}
              list={images}
              onImageTabChange={this.handleImageTabChange.bind(this)}
              onImageClick={this.handleImageClick.bind(this)}
            />
            <input type="hidden" name="image" value={this.state.image} readOnly="false"/>
          </div>
          <div className="droplet-size">
            <SizePanel
              tabIndex={this.state.sizeTab}
              value={this.state.size}
              list={sizes}
              onSizeTabChange={this.handleSizeTabChange.bind(this)}
              onSizeClick={this.handleSizeClick.bind(this)}
            />
            <input type="hidden" name="size" value={this.state.size} readOnly="false"/>
          </div>
          <div className="droplet-datacenter-region">
            <DataCenterRegionPanel
              region={this.state.region}
              site={this.state.site}
              list={dataCenterRegions}
              onDataCenterRegionClick={this.handleDataCenterRegionClick.bind(this)}
            />
            <input type="hidden" name="region" value={this.state.region} readOnly="false"/>
            <input type="hidden" name="site" value={this.state.site} readOnly="false"/>
          </div>
          <div className="droplet-additional-options">
            <div className="additional-options-panel">
              <div className="page-header">
                <h3>Select additional options</h3>
              </div>
              {additionalOptions}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ReactDOM.render(
  <Droplet />,
  document.getElementById('root')
);
